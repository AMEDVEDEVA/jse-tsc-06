package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.golovina.tm.api.service.IProjectService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    private IProjectService projectService;

    public ProjectEndpoint() {

    }

    public ProjectEndpoint(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    @WebMethod
    public @NotNull List<Project> findAllByUserId(@Nullable @WebParam(name = "userId") String userId) {
        return projectService.findAll(userId);
    }

    @Override
    @WebMethod
    public @NotNull Project findByIdUserId(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    ) {
        return projectService.findById(userId, id);
    }

    @Override
    @WebMethod
    public @NotNull Project findByIndexUserId(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        return projectService.findByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void clearByUserId(@Nullable @WebParam(name = "userId") String userId) {
        projectService.clear(userId);
    }

    @Override
    @WebMethod
    public void removeByUserId(
            @NotNull @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "entity") Project entity
    ) {
        projectService.remove(userId, entity);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSizeByUserId(@NotNull @WebParam(name = "userId") String userId) {
        return projectService.getSize(userId);
    }

    @Override
    @WebMethod
    public void add(@Nullable @WebParam(name = "entity") Project entity) {
        projectService.add(entity);
    }

    @Override
    @WebMethod
    public void addAll(
            @Nullable @WebParam(name = "entities", partName = "entities") List<Project> entities
    ) {
        projectService.addAll(entities);
    }

    @Override
    @WebMethod
    public void remove(@Nullable @WebParam(name = "entity") Project entity) {
        projectService.remove(entity);
    }

    @Override
    @WebMethod
    public @NotNull List<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    public void clear() {
        projectService.clear();
    }

    @Override
    @WebMethod
    public @NotNull Project findById(@Nullable @WebParam(name = "id") String id) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    public @NotNull Project findByIndex(@NotNull @WebParam(name = "index") Integer index) {
        return projectService.findByIndex(index);
    }

    @Override
    @WebMethod
    public boolean existsById(@Nullable @WebParam(name = "id") String id) {
        return projectService.existsById(id);
    }

    @Override
    @WebMethod
    public boolean existsByIndex(@Nullable @WebParam(name = "index") Integer index) {
        return projectService.existsByIndex(index);
    }

    @Override
    @WebMethod
    public @NotNull Project removeById(@NotNull @WebParam(name = "id") String id) {
        return projectService.removeById(id);
    }

    @Override
    @WebMethod
    public @NotNull Project removeByIndex(@Nullable @WebParam(name = "index") Integer index) {
        return projectService.removeByIndex(index);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSize() {
        return projectService.getSize();
    }

    @Override
    @WebMethod
    public void create(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        projectService.create(userId, name);
    }

    @Override
    @WebMethod
    public void createWithDescription(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        projectService.create(userId, name, description);
    }

    @Override
    @WebMethod
    public @NotNull Project findByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        return projectService.findByName(userId, name);
    }

    @Override
    @WebMethod
    public void updateById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description) {
        projectService.updateById(userId, id, name, description);
    }

    @Override
    @WebMethod
    public void updateByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) {
        projectService.updateByIndex(userId, index, name, description);
    }

    @Override
    @WebMethod
    public boolean existsByName(
            @NotNull @WebParam(name = "userId") String userId,
            @NotNull @WebParam(name = "name") String name
    ) {
        return projectService.existsByName(userId, name);
    }

    @Override
    @WebMethod
    public @NotNull Project startById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    ) {
        return projectService.startById(userId, id);
    }

    @Override
    @WebMethod
    public @NotNull Project startByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        return projectService.startByIndex(userId, index);
    }

    @Override
    @WebMethod
    public @NotNull Project startByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        return projectService.startByName(userId, name);
    }

    @Override
    @WebMethod
    public @NotNull Project finishById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    ) {
        return projectService.finishById(userId, id);
    }

    @Override
    @WebMethod
    public @NotNull Project finishByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        return projectService.finishByIndex(userId, index);
    }

    @Override
    @WebMethod
    public @NotNull Project finishByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        return projectService.finishByName(userId, name);
    }

    @Override
    @WebMethod
    public @NotNull Project changeStatusById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    ) {
        return projectService.changeStatusById(userId, id, status);
    }

    @Override
    @WebMethod
    public @NotNull Project changeStatusByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    ) {
        return projectService.changeStatusByIndex(userId, index, status);
    }

    @Override
    @WebMethod
    public @NotNull Project changeStatusByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    ) {
        return projectService.changeStatusByName(userId, name, status);
    }

}
