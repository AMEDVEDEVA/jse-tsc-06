package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminUserEndpoint {

    @WebMethod
    User lockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    User removeUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    User removeUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    User unlockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "login", partName = "login") String login
    );

    User setUserRole(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "role", partName = "role") Role role
    );

}
