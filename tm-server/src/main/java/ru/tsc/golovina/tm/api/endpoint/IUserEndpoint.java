package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @WebMethod
    void add(@NotNull @WebParam(name = "entity") User entity);

    @WebMethod
    void addAll(@NotNull @WebParam(name = "entities") List<User> entities);

    @WebMethod
    void remove(@NotNull @WebParam(name = "entity") User entity);

    @WebMethod
    @NotNull List<User> findAll();

    @WebMethod
    boolean existsById(@NotNull @WebParam(name = "id") String id);

    @WebMethod
    boolean existsByIndex(@NotNull @WebParam(name = "index") Integer index);

    @WebMethod
    void clear();

    @WebMethod
    @NotNull User findById(@NotNull @WebParam(name = "id") String id);

    @WebMethod
    @Nullable User findByIndex(@NotNull @WebParam(name = "index") Integer index);

    @WebMethod
    @Nullable User removeById(@NotNull @WebParam(name = "id") String id);

    @WebMethod
    @Nullable User removeByIndex(@NotNull @WebParam(name = "index") Integer index);

    @WebMethod
    @NotNull Integer getSize();

    @WebMethod
    @NotNull User create(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password
    );

    @WebMethod
    @NotNull User createwithEmail(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "email") String email
    );

    @WebMethod
    @NotNull User createwithRole(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "role") Role role
    );

    @WebMethod
    void setPassword(
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "password") String password
    );

    @WebMethod
    void setRole(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "role") Role role
    );

    @WebMethod
    @NotNull User findUserByLogin(@Nullable @WebParam(name = "login") String login);

    @WebMethod
    @NotNull User findUserByEmail(@Nullable @WebParam(name = "email") String email);

    @WebMethod
    void removeUserById(@Nullable @WebParam(name = "id") String id);

    @WebMethod
    @NotNull User removeUserByLogin(@NotNull @WebParam(name = "login") String login);

    @WebMethod
    boolean isLoginExists(@NotNull @WebParam(name = "login") String login);

    @WebMethod
    boolean isEmailExists(@NotNull @WebParam(name = "email") String email);

    @WebMethod
    void updateUserById(
            @Nullable String id,
            @Nullable @WebParam(name = "lastName") String lastName,
            @Nullable @WebParam(name = "firstName") String firstName,
            @Nullable @WebParam(name = "middleName") String middleName,
            @Nullable @WebParam(name = "email") String email
    );

    @WebMethod
    void updateUserByLogin(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "lastName") String lastName,
            @Nullable @WebParam(name = "firstName") String firstName,
            @Nullable @WebParam(name = "middleName") String middleName,
            @Nullable @WebParam(name = "email") String email
    );

    @WebMethod
    void lockUserByLogin(@Nullable @WebParam(name = "login") String login);

    @WebMethod
    void unlockUserByLogin(@Nullable @WebParam(name = "login") String login);

}
