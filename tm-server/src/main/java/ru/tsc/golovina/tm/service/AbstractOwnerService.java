package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IOwnerRepository;
import ru.tsc.golovina.tm.api.service.IOwnerService;
import ru.tsc.golovina.tm.exception.empty.EmptyIdException;
import ru.tsc.golovina.tm.exception.empty.EmptyIndexException;
import ru.tsc.golovina.tm.exception.empty.EmptyUserIdException;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.golovina.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    @NotNull
    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(@NotNull final IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final E entity = repository.findById(userId, id);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null) throw new EmptyIndexException();
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    public void remove(@NotNull final String userId, @Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(userId, entity);
    }

    @NotNull
    @Override
    public Integer getSize(@NotNull final String userId) {
        return repository.getSize(userId);
    }


}
