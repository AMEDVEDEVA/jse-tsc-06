package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.golovina.tm.api.service.ITaskService;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint implements ITaskEndpoint {

    private ITaskService taskService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    @WebMethod
    public void add(@NotNull @WebParam(name = "entity") Task entity) {
        taskService.add(entity);
    }

    @Override
    @WebMethod
    public void addAll(@NotNull @WebParam(name = "entities") List<Task> entities) {
        taskService.addAll(entities);
    }

    @Override
    @WebMethod
    public void remove(@NotNull @WebParam(name = "entity") Task entity) {
        taskService.remove(entity);
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    public boolean existsById(@NotNull @WebParam(name = "id") String id) {
        return taskService.existsById(id);
    }

    @Override
    @WebMethod
    public boolean existsByIndex(@NotNull @WebParam(name = "index") Integer index) {
        return taskService.existsByIndex(index);
    }

    @Override
    @WebMethod
    public void clear() {
        taskService.clear();
    }

    @Override
    @WebMethod
    public @NotNull Task findById(@NotNull @WebParam(name = "id") String id) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    public @Nullable Task findByIndex(@NotNull @WebParam(name = "index") Integer index) {
        return taskService.findByIndex(index);
    }

    @Override
    @WebMethod
    public @Nullable Task removeById(@NotNull @WebParam(name = "id") String id) {
        return taskService.removeById(id);
    }

    @Override
    @WebMethod
    public @Nullable Task removeByIndex(@NotNull @WebParam(name = "index") Integer index) {
        return taskService.removeByIndex(index);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSize() {
        return taskService.getSize();
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAllByUserId(@NotNull @WebParam(name = "userId") String userId) {
        return taskService.findAll(userId);
    }

    @Override
    @WebMethod
    public @NotNull Task findByIdUSerId(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    ) {
        return taskService.findById(userId, id);
    }

    @Override
    @WebMethod
    public @NotNull Task findByIndexUserId(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        return taskService.findByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void clearByUserId(@Nullable @WebParam(name = "userId") String userId) {
        taskService.clear(userId);
    }

    @Override
    @WebMethod
    public void removeByUserId(
            @NotNull @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "entity") Task entity
    ) {
        taskService.remove(userId, entity);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSize(@NotNull @WebParam(name = "userId") String userId) {
        return taskService.getSize(userId);
    }

    @Override
    @WebMethod
    public void create(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        taskService.create(userId, name);
    }

    @Override
    @WebMethod
    public void create(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        taskService.create(userId, name, description);
    }

    @Override
    @WebMethod
    public @NotNull Task removeByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        return taskService.removeByName(userId, name);
    }

    @Override
    @WebMethod
    public void updateById(
            @NotNull @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        taskService.updateById(userId, id, name, description);
    }

    @Override
    @WebMethod
    public void updateByIndex(
            @NotNull @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        taskService.updateByIndex(userId, index, name, description);
    }

    @Override
    @WebMethod
    public boolean existsByName(
            @Nullable @WebParam(name = "userId") String userId,
            @NotNull @WebParam(name = "name") String name
    ) {
        return taskService.existsByName(userId, name);
    }

    @Override
    @WebMethod
    public @NotNull Task findByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        return taskService.findByName(userId, name);
    }

    @Override
    @WebMethod
    public @NotNull Task startById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    ) {
        return taskService.startById(userId, id);
    }

    @Override
    @WebMethod
    public @NotNull Task startByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        return taskService.startByIndex(userId, index);
    }

    @Override
    @WebMethod
    public @NotNull Task startByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        return taskService.startByName(userId, name);
    }

    @Override
    @WebMethod
    public @NotNull Task finishById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    ) {
        return taskService.finishById(userId, id);
    }

    @Override
    @WebMethod
    public @NotNull Task finishByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        return taskService.finishByIndex(userId, index);
    }

    @Override
    @WebMethod
    public @NotNull Task finishByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    ) {
        return taskService.finishByName(userId, name);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    ) {
        return taskService.changeStatusById(userId, id, status);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    ) {
        return taskService.changeStatusByIndex(userId, index, status);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    ) {
        return taskService.changeStatusByName(userId, name, status);
    }

}
