package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.repository.ISessionRepository;
import ru.tsc.golovina.tm.model.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean contains(@NotNull final String id) {
        return list.stream().anyMatch(e -> id.equals(e.getId()));
    }

}
