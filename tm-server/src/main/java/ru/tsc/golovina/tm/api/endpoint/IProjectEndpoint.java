package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    @NotNull List<Project> findAllByUserId(@Nullable @WebParam(name = "userId") String userId);

    @WebMethod
    @NotNull Project findByIdUserId(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project findByIndexUserId(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    void clearByUserId(@Nullable @WebParam(name = "userId") String userId);

    @WebMethod
    void removeByUserId(
            @NotNull @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "entity") Project entity
    );

    @WebMethod
    @NotNull Integer getSizeByUserId(@NotNull @WebParam(name = "userId") String userId);

    @WebMethod
    void add(@Nullable @WebParam(name = "entity") Project entity);

    @WebMethod
    void addAll(
            @Nullable @WebParam(name = "entities", partName = "entities") List<Project> entities
    );

    @WebMethod
    void remove(@Nullable @WebParam(name = "entity") Project entity);

    @WebMethod
    @NotNull List<Project> findAll();

    @WebMethod
    void clear();

    @WebMethod
    @NotNull Project findById(@Nullable @WebParam(name = "id") String id);

    @WebMethod
    @NotNull Project findByIndex(@NotNull @WebParam(name = "index") Integer index);

    @WebMethod
    boolean existsById(@Nullable @WebParam(name = "id") String id);

    @WebMethod
    boolean existsByIndex(@Nullable @WebParam(name = "index") Integer index);

    @WebMethod
    @NotNull Project removeById(@NotNull @WebParam(name = "id") String id);

    @WebMethod
    @NotNull Project removeByIndex(@Nullable @WebParam(name = "index") Integer index);

    @WebMethod
    @NotNull Integer getSize();

    @WebMethod
    void create(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void createWithDescription(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    @NotNull Project findByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void updateById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description);

    @WebMethod
    void updateByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    );

    @WebMethod
    boolean existsByName(
            @NotNull @WebParam(name = "userId") String userId,
            @NotNull @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Project startById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project startByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Project startByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Project finishById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project finishByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Project finishByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Project changeStatusById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Project changeStatusByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Project changeStatusByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    );

}
