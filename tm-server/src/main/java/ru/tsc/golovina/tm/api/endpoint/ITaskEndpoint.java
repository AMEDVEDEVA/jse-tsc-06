package ru.tsc.golovina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void add(@NotNull @WebParam(name = "entity") Task entity);

    @WebMethod
    void addAll(@NotNull @WebParam(name = "entities") List<Task> entities);

    @WebMethod
    void remove(@NotNull @WebParam(name = "entity") Task entity);

    @WebMethod
    @NotNull List<Task> findAll();

    @WebMethod
    boolean existsById(@NotNull @WebParam(name = "id") String id);

    @WebMethod
    boolean existsByIndex(@NotNull @WebParam(name = "index") Integer index);

    @WebMethod
    void clear();

    @WebMethod
    @NotNull Task findById(@NotNull @WebParam(name = "id") String id);

    @WebMethod
    @Nullable Task findByIndex(@NotNull @WebParam(name = "index") Integer index);

    @WebMethod
    @Nullable Task removeById(@NotNull @WebParam(name = "id") String id);

    @WebMethod
    @Nullable Task removeByIndex(@NotNull @WebParam(name = "index") Integer index);

    @WebMethod
    @NotNull Integer getSize();

    @WebMethod
    @NotNull List<Task> findAllByUserId(@NotNull @WebParam(name = "userId") String userId);

    @WebMethod
    @NotNull Task findByIdUSerId(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task findByIndexUserId(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    void clearByUserId(@Nullable @WebParam(name = "userId") String userId);

    @WebMethod
    void removeByUserId(
            @NotNull @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "entity") Task entity
    );

    @WebMethod
    @NotNull Integer getSize(@NotNull @WebParam(name = "userId") String userId);

    @WebMethod
    void create(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void create(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    @NotNull Task removeByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void updateById(
            @NotNull @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    void updateByIndex(
            @NotNull @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    boolean existsByName(
            @Nullable @WebParam(name = "userId") String userId,
            @NotNull @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task findByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task startById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task startByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task startByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task finishById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task finishByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task finishByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task changeStatusById(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Task changeStatusByIndex(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Task changeStatusByName(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    );

}
