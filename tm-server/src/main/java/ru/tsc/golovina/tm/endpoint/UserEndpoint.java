package ru.tsc.golovina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.endpoint.IUserEndpoint;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint implements IUserEndpoint {

    private IUserService userService;

    public UserEndpoint() {
    }

    public UserEndpoint(IUserService userService) {
        this.userService = userService;
    }

    @Override
    @WebMethod
    public void add(@NotNull @WebParam(name = "entity") User entity) {
        userService.add(entity);
    }

    @Override
    @WebMethod
    public void addAll(@NotNull @WebParam(name = "entities") List<User> entities) {
        userService.addAll(entities);
    }

    @Override
    @WebMethod
    public void remove(@NotNull @WebParam(name = "entity") User entity) {
        userService.remove(entity);
    }

    @Override
    @WebMethod
    public @NotNull List<User> findAll() {
        return userService.findAll();
    }

    @Override
    @WebMethod
    public boolean existsById(@NotNull @WebParam(name = "id") String id) {
        return userService.existsById(id);
    }

    @Override
    @WebMethod
    public boolean existsByIndex(@NotNull @WebParam(name = "index") Integer index) {
        return userService.existsByIndex(index);
    }

    @Override
    @WebMethod
    public void clear() {
        userService.clear();
    }

    @Override
    @WebMethod
    public @NotNull User findById(@NotNull @WebParam(name = "id") String id) {
        return userService.findById(id);
    }

    @Override
    @WebMethod
    public @Nullable User findByIndex(@NotNull @WebParam(name = "index") Integer index) {
        return userService.findByIndex(index);
    }

    @Override
    @WebMethod
    public @Nullable User removeById(@NotNull @WebParam(name = "id") String id) {
        return userService.removeById(id);
    }

    @Override
    @WebMethod
    public @Nullable User removeByIndex(@NotNull @WebParam(name = "index") Integer index) {
        return userService.removeByIndex(index);
    }

    @Override
    @WebMethod
    public @NotNull Integer getSize() {
        return userService.getSize();
    }

    @Override
    @WebMethod
    public @NotNull User create(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password
    ) {
        return userService.create(login, password);
    }

    @Override
    @WebMethod
    public @NotNull User createwithEmail(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "email") String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    @WebMethod
    public @NotNull User createwithRole(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "role") Role role
    ) {
        return userService.create(login, password, role);
    }

    @Override
    @WebMethod
    public void setPassword(
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "password") String password
    ) {
        userService.setPassword(id, password);
    }

    @Override
    @WebMethod
    public void setRole(
            @Nullable @WebParam(name = "userId") String userId,
            @Nullable @WebParam(name = "role") Role role
    ) {
        userService.setRole(userId, role);
    }

    @Override
    @WebMethod
    public @NotNull User findUserByLogin(@Nullable @WebParam(name = "login") String login) {
        return userService.findUserByLogin(login);
    }

    @Override
    @WebMethod
    public @NotNull User findUserByEmail(@Nullable @WebParam(name = "email") String email) {
        return userService.findUserByEmail(email);
    }

    @Override
    @WebMethod
    public void removeUserById(@Nullable @WebParam(name = "id") String id) {
        userService.removeUserById(id);
    }

    @Override
    @WebMethod
    public @NotNull User removeUserByLogin(@NotNull @WebParam(name = "login") String login) {
        return userService.removeUserByLogin(login);
    }

    @Override
    @WebMethod
    public boolean isLoginExists(@NotNull @WebParam(name = "login") String login) {
        return userService.isLoginExists(login);
    }

    @Override
    @WebMethod
    public boolean isEmailExists(@NotNull @WebParam(name = "email") String email) {
        return userService.isEmailExists(email);
    }

    @Override
    @WebMethod
    public void updateUserById(
            @Nullable String id,
            @Nullable @WebParam(name = "lastName") String lastName,
            @Nullable @WebParam(name = "firstName") String firstName,
            @Nullable @WebParam(name = "middleName") String middleName,
            @Nullable @WebParam(name = "email") String email
    ) {
        userService.updateUserById(id, lastName, firstName, middleName, email);
    }

    @Override
    @WebMethod
    public void updateUserByLogin(
            @Nullable @WebParam(name = "login") String login,
            @Nullable @WebParam(name = "lastName") String lastName,
            @Nullable @WebParam(name = "firstName") String firstName,
            @Nullable @WebParam(name = "middleName") String middleName,
            @Nullable @WebParam(name = "email") String email
    ) {
        userService.updateUserByLogin(login, lastName, firstName, middleName, email);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(@Nullable @WebParam(name = "login") String login) {
        userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(@Nullable @WebParam(name = "login") String login) {
        userService.unlockUserByLogin(login);
    }

}
