package ru.tsc.golovina.tm.api;

import ru.tsc.golovina.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
