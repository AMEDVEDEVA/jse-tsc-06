package ru.tsc.golovina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.endpoint.Role;
import ru.tsc.golovina.tm.endpoint.Session;

public class BackupSaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "backup-save";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Save backup to XML.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        serviceLocator.getAdminDataEndpoint().saveBackup(session);
    }

    @Nullable
    public Role[] roles() {
        if (serviceLocator.getSession() == null || Thread.currentThread().getName().equals("DataThread")) {
            return null;
        } else {
            return new Role[]{Role.ADMIN};
        }
    }

}
